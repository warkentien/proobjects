package P8_15;

/**
 * Created by sethf_000 on 10/18/2015.
 */
public class Sphere {

    //return the calculation for volume of a sphere
    public static double sphereVolume(double r){
        return (4.0 / 3.0) * Math.PI * (r * r * r);
    }

    //return the calculation for surface area of a sphere
    public static double sphereSurface(double r){
        return 4 * Math.PI * r * r;
    }

}
