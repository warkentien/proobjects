package P8_15;

import java.util.Scanner;

/**
 * Created by sethf_000 on 10/18/2015.
 *
 * The user enters values for height and radius and the prgram calls methods in the Sphere
 * Cone and Cylinder classes to calculate volume and surface area for each shape.
 *
 * This method is more object-oriented than the solution in P 8.14 because it puts each of the
 * individual shapes into their own class. This is more intuitive because when we create an object
 * of these classes, it is a better and more specific programatic representation of the real-world
 * shapes we are modeling.
 */
public class Driver {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //create new sphere cylinder and cone objects
        Sphere sphere = new Sphere();
        Cylinder cylinder = new Cylinder();
        Cone cone = new Cone();

        //get the height and radius from user
        System.out.print("Enter the value for the height: ");
        double h = scan.nextDouble();
        System.out.print("Enter the value for the radius: ");
        double r = scan.nextDouble();

        //print each of the calculations by calling the methods in the individual classes
        System.out.println("The volume of the sphere is: " + sphere.sphereVolume(r));
        System.out.println("The surface area of the sphere is: " + sphere.sphereSurface(r));

        System.out.println("The volume of the cylinder is: " + cylinder.cylinderVolume(r, h));
        System.out.println("The surface area of the cylinder is: " + cylinder.cylinderSurface(r, h));

        System.out.println("The volume of the cone is: " + cone.coneVolume(r, h));
        System.out.println("The surface area of the cone is: " + cone.coneSurface(r, h));
    }
}
