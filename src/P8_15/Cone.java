package P8_15;

/**
 * Created by sethf_000 on 10/18/2015.
 */
public class Cone {

    //return the calculation for volume of a cone
    public static double coneVolume(double r, double h){
        return Math.PI * r * r * (h/3);
    }

    //return the calculation for surface area of a cone
    public static double coneSurface(double r, double h){
        return Math.PI * r * (r + Math.sqrt((h * h) + (r * r)));
    }

}
