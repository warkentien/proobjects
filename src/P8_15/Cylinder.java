package P8_15;

/**
 * Created by sethf_000 on 10/18/2015.
 */
public class Cylinder {

    //return the calculation for volume of a cylinder
    public static double cylinderVolume(double r, double h){
        return Math.PI * r * r * h;
    }

    //return the calculation for surface area of a cylinder
    public static double cylinderSurface(double r, double h){
        return (2 * Math.PI * r * h) + (2 * Math.PI * r * r);
    }

}
