package P8_8;

/**
 * Created by sethf_000 on 10/17/2015.
 *
 * The program creates a student object and then adds grades by number and/or by letter
 * grade.
 */
public class Driver {
    public static void main(String[] args) {

        //create new student object
        Student student1 = new Student("Jim Andrews");

        //add different numeric and letter grades
        student1.addScore(93);
        student1.addScore(89);
        student1.addGrade("A+");
        student1.addGrade("B-");
        student1.addGrade("A-");

        //print out student name, plus various grade data
        System.out.println(student1.getName());
        System.out.println("Total score: " +  student1.getTotalScore());
        System.out.println("Average score: " + student1.getAverageScore());
        System.out.println("GPA: " + student1.getGPA());

    }
}