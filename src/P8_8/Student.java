package P8_8;

/**
 * Created by sethf_000 on 10/17/2015.
 *
 * This class takes in the student's name and grades either in letter-grade format or just by a
 * score out of 100. In either case, we translate from letter grade to numeric score or vice versa
 * so that we can either send a letter grade to Grade class and get gpa, or get the total and average
 * scores by number.
 */
public class Student {

    //create the fields for this class
    //student's name
    private String name;
    //running total of numeric score
    private double totalScore;
    //counts the number of new grades
    private int count = 0;
    //create Grade instance
    private Grade grade = new Grade();

    //set student name
    public Student(String name){
        this.name = name;
    }

    //add a numeric score
    public void addScore(double score) {
        String newGrade = "";

        //translate numeric score to letter grade
        if(score >= 97 ){
            newGrade = "A+";
        }
        else if(score >= 93 ){
            newGrade = "A";
        }
        else if(score >= 90 ){
            newGrade = "A-";
        }
        else if(score >= 87 ){
            newGrade = "B+";
        }
        else if(score >= 83 ){
            newGrade = "B";
        }
        else if(score >= 80 ){
            newGrade = "B-";
        }
        else if(score >= 77 ){
            newGrade = "C+";
        }
        else if(score >= 73 ){
            newGrade = "C";
        }
        else if(score >= 70 ){
            newGrade = "C-";
        }
        else if(score >= 67 ){
            newGrade = "D+";
        }
        else if(score >= 65 ){
            newGrade = "D";
        }
        else if(score < 60 ){
            newGrade = "F";
        }

        //send grade as letter grade
        grade.addGrade(newGrade);
        this.totalScore += score;
        this.count += 1;
    }

    //Add a letter grade
    public void addGrade(String newGrade) {

        //translate letter grade to numeric score
        if (newGrade.equals("A+")){
            this.totalScore += 100;
        }
        else if (newGrade.equals("A")){
            this.totalScore += 97;
        }
        else if (newGrade.equals("A-")){
            this.totalScore += 93;
        }
        else if (newGrade.equals("B+")){
            this.totalScore += 90;
        }
        else if (newGrade.equals("B")){
            this.totalScore += 87;
        }
        else if (newGrade.equals("B-")){
            this.totalScore += 83;
        }
        else if (newGrade.equals("C+")){
            this.totalScore += 80;
        }
        else if (newGrade.equals("C")){
            this.totalScore += 77;
        }
        else if (newGrade.equals("C-")){
            this.totalScore += 73;
        }
        else if (newGrade.equals("D+")){
            this.totalScore += 70;
        }
        else if (newGrade.equals("D")){
            this.totalScore += 65;
        }
        else if (newGrade.equals("F")){
            this.totalScore += 60;
        }

        grade.addGrade(newGrade);
        this.count += 1;
    }

    //return student's name
    public String getName() {
        return this.name;
    }

    //return total score
    public double getTotalScore() {
        return this.totalScore;
    }

    //return average score
    public double getAverageScore() {
        return this.totalScore / this.count;
    }

    //return GPA from grade object
    public double getGPA() {
        return grade.getNumericGrade();
    }

}
