package P8_8;

/**
 * Created by sethf_000 on 10/17/2015.
 *
 * This class takes in a letter grade and keeps track of how many grades were added. The addGrade
 * method will translate the grade to a numeric value on a 4.0 grading scale. The getNumericGrade
 * method divides a total running numeric value for all grades by the number of grades entered
 * in order to return a total gpa for all grades.
 */
public class Grade {

    //the letter grade
    private String grade;
    //running total for gpa
    private double numericGrade;
    //number of grades added
    private int count = 0;

    //empty constructor to creat grade instance
    public Grade() {
    }

    //allows for grade to be added when creating new grade object
    public Grade(String grade) {
        this.grade = grade;
        addGrade(grade);
        this.count++;
    }

    //computes the current gpa
    public double getNumericGrade() {
        return (this.numericGrade/this.count);
    }

    //adds to running gpa total when letter grade is sent in
    public void addGrade(String grade){

        if (grade.equals("A+")){
            this.numericGrade += 4.0;
        }
        else if (grade.equals("A")){
            this.numericGrade += 4.0;
        }
        else if (grade.equals("A-")){
            this.numericGrade += 3.7;
        }
        else if (grade.equals("B+")){
            this.numericGrade += 3.3;
        }
        else if (grade.equals("B")){
            this.numericGrade += 3.0;
        }
        else if (grade.equals("B-")){
            this.numericGrade += 2.7;
        }
        else if (grade.equals("C+")){
            this.numericGrade += 2.3;
        }
        else if (grade.equals("C")){
            this.numericGrade += 2.0;
        }
        else if (grade.equals("C-")){
            this.numericGrade += 1.7;
        }
        else if (grade.equals("D+")){
            this.numericGrade += 1.3;
        }
        else if (grade.equals("D")){
            this.numericGrade += 1.0;
        }
        else if (grade.equals("F")){
            this.numericGrade += 0.0;
        }
        this.count++;
    }

}
