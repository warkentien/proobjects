package P8_11;

import java.util.Scanner;

/**
 * Created by sethf_000 on 10/18/2015.
 *
 * This program creates a Letter object that is intialized with a sender and recipient.
 * The user can then enter two lines of text for the letter, which is then printed out
 * in a formatted way.
 */
public class Driver {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //create a new Letter object and initialize the to and from people
        Letter newLetter = new Letter("Sandy", "Jim");

        //get the first  line of the letter
        System.out.print("Enter the first line of the letter: ");
        //send it to addLine method
        newLetter.addLine(scan.nextLine());
        //get the next line of the letter
        System.out.print("Enter the next line of the letter: ");
        //send it to addLine method
        newLetter.addLine(scan.nextLine());

        System.out.println();
        //print out the formatted letter by calling getText method
        System.out.println(newLetter.getText());

    }
}
