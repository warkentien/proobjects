package P8_11;

/**
 * Created by sethf_000 on 10/18/2015.
 *
 * This class takes in sender and recipient information from the Driver class,
 * as well as lines to be included in the letter. It concatenates all of the information
 * into a single string, which is then returned formatted as the letter.
 */
public class Letter {
    //set our fields
    private String from;
    private String to;
    //start off the text of the letter with Dear
    private String text = "Dear ";


    public Letter(String from, String to){
        //set sender variable
        this.from = from;
        //set recipient variable
        this.to = to;
        //concatenate recipient info onto our letter text
        this.text = this.text + this.to + "," + "\n \n";
    }

    public void addLine(String line){
        //concatenate each line onto our letter text
        this.text = this.text + line + "\n";
    }

    public String getText(){
        //return the letter text with salutation and sender info
        return this.text + "\n" + "Sincerely, " + "\n" + this.from;
    }
}
