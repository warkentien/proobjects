package P8_4;

/**
 * Created by sethf_000 on 10/16/2015.
 *
 * This class has two constructors to set the address information. One has the option for an
 * apartment number while the other does not.  The print method prints the address in a
 * formatted way, while the comesBefore method checks if one zip code is less than another.
 */
public class Address {

    //declare the fields for this class
    private String houseNumber = "";
    private String street = "";
    private String apartmentNo = "";
    private String city = "";
    private String state = "";
    private int postCode = 0;


    //create first constructor, which takes in an optional apt. no.
    public Address(String houseNumber, String street, String apartmentNo, String city,
            String state, int postCode){
        //populate the variables on object creation
        this.houseNumber = houseNumber;
        this.street = street;
        this.apartmentNo = apartmentNo;
        this.city = city;
        this.state = state;
        this.postCode = postCode;

    }

    //create second constructor, which does not take in an apt. no.
    public Address(String houseNumber, String street, String city, String state, int postCode){
        //populate the variables on object creation
        this.houseNumber = houseNumber;
        this.street = street;
        this.city = city;
        this.state = state;
        this.postCode = postCode;

    }

    //print method that prints the object's info in a formatted way
    public void print(){
        System.out.println(houseNumber + " " + street + " " + apartmentNo);
        System.out.println(city + " " + state +  " " + postCode);
        System.out.println();
    }

    //check if one zip comes before another, return boolean value
    public boolean comesBefore(Address compareAddress){
        if(this.postCode < compareAddress.postCode){
            return true;
        }
        else{
            return false;
        }

    }

}
