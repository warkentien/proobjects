package P8_4;

/**
 * Created by sethf_000 on 10/16/2015.
 *
 * This program creates two Address objects, one of which is initialized with an address that has an
 * apartment number, and one that does not. the program prints out the objects' addresses in a formatted
 * manner and checks the zip codes to determine whether or not one comes before the other (is less than).
 */
public class Driver {
    public static void main(String[] args) {
        //create two Address objects, sending info to the two different constructors
        Address address1 = new Address("1426", "W. Glenn", "Apt. 2", "Chicago", "IL", 60660);
        Address address2 = new Address("1119", "N. Main Street", "Indianapolis", "IN", 46052);

        //call the print method on both objects, which prints the address in a formatted way
        address1.print();
        address2.print();

        //call comesBefore method, which compares if one zip comes before the other
        //I have elected to print the boolean result
        System.out.println(address1.comesBefore(address2));

    }
}
