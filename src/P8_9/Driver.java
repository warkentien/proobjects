package P8_9;

import java.util.Scanner;

/**
 * Created by sethf_000 on 10/18/2015.
 *
 * This class creates a lock object initialized with 3 secret numbers. The user is able to
 * enter three "dials" (which are not the same as the secret numbers), one at a time, to try
 * and unlock the lock. After entering three numbers, if any of them did not result in hitting the
 * secret number, the lock is reset and the user is able to start over from the first number. The loop
 * will only end when all three numbers have been hit.
 */
public class Driver {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //create lock object and set it's code (problem is unclear on whether the user should set this code)
        ComboLock lock = new ComboLock(38, 3, 39);

        //stay in while loop while the lock is still locked
        //note: to unlock it as is, enter: 2, 5, 4
        while(!lock.open()){

            //reset the dial to zero on each iteration
            lock.reset();
            System.out.println();
            System.out.println("The lock has been set to 0");
            //get first ticks to right
            System.out.print("How many first ticks to the right?: ");
            int first = scan.nextInt();
            //see if this is the right number of ticks
            lock.turnRight(first);
            //get ticks to left
            System.out.print("How many ticks to the left?: ");
            int second = scan.nextInt();
            //check ticks
            lock.turnLeft(second);
            //get last ticks
            System.out.print("How many final ticks to the right?: ");
            int third = scan.nextInt();
            //check them
            lock.turnRight(third);

        }

        System.out.println();
        System.out.println("You have unlocked the lock!");

    }
}