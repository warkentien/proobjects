package P8_9;

/**
 * Created by sethf_000 on 10/18/2015.
 *
 * This class sets the three secret numbers when the constructor is called. There are two
 * methods that turn the dial right and left. We subtract and add from 40 to see if the correct
 * number of ticks were entered in order to hit the secret number. Booleans are used to keep track
 * of whether or not a number was hit. The lock will only open if all three booleans get set to true.
 */
public class ComboLock {

    //set the fields
    private int dial = 0;
    //the three codes
    private int secret1;
    private int secret2;
    private int secret3;
    //first ticks to the right
    private int firstTicks;
    //ticks to the left
    private int secondTicks;
    //final ticks to the right
    private int thirdTicks;
    //booleans to check if each number is hit
    private boolean first;
    private boolean second;
    private boolean third;
    //flag for the turnRight method to distinguish between first and third ticks
    private boolean firstRight = true;

    //set the lock
    public ComboLock(int secret1, int secret2, int secret3) {
        this.secret1 = secret1;
        this.secret2 = secret2;
        this.secret3 = secret3;
    }
    //reset dial
    public void reset() {
        dial = 0;
    }

    public void turnRight(int ticks) {

        //check to see if this is the first turnRight call or the second
        if(firstRight){
            //get how many ticks were sent in
            this.firstTicks = ticks;
            //check if we hit our first secret
            if(40 - ticks == secret1){
                first = true;
            }
        }
        //if it's the second call to turnRight
        else {
            this.secondTicks= ticks;
            if((40 + secret2) - ticks == secret3){
                third = true;
            }
        }
        //switch our flag boolean
        firstRight = !firstRight;
    }

    public void turnLeft(int ticks) {
        //see if we hit the second secret
        if (secret2 + firstTicks - ticks == 0){
            second = true;
        }
    }

    public boolean open() {
        //if all three numbers were hit, end the while loop in Driver
        if (first && second && third){
            return true;
        }
        else {
            return false;
        }
    }

}
