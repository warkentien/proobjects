package P8_5;

import javax.sound.midi.Soundbank;

/**
 * Created by sethf_000 on 10/17/2015.
 *
 * This program creates a SodaCan object with a specified height and radius. It then prints out
 * the surface area and volume of the can based on the parameters of the object.
 */
//Driver class for SodaCan problem
public class Driver {
    public static void main(String[] args) {

        //create SodaCan object and pass in a height and radius
        SodaCan can = new SodaCan(4.75, 1.31);

        //print out the surface area of the can by calling the getSurfaceArea method
        System.out.println("The surface area of the can is: " + can.getSurfaceArea() + " sq. in.");

        //print out the volume of the can by calling the getVolume method
        System.out.println("The volume of the can is: " + can.getVolume() + " cubic in.");
    }
}
