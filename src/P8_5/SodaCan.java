package P8_5;

/**
 * Created by sethf_000 on 10/17/2015.
 *
 * The constructor receives height and radius, and the two methods use these values
 * to compute either surface area of volume
 */
public class SodaCan {

    //declare the fields for this class
    private double height;
    private double radius;

    //constructor that sets the height and radius on object creation
    public SodaCan(double height, double radius){
        this.height = height;
        this.radius = radius;
    }

    //method that computes the surface area using formula for surface area of a cylindar
    public double getSurfaceArea(){
        return (2.0 * radius * Math.PI * height) + (2.0 * Math.PI * radius * radius);
    }

    //method that computes volume using formula for volume of a cylindar
    public double getVolume(){
        return height * Math.PI * radius * radius;
    }
}
