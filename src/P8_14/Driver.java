package P8_14;

import java.util.Scanner;

/**
 * Created by sethf_000 on 10/18/2015.
 *
 * Note: the problem wasn't clear whether we should put the executable code in the Geometry class
 * or not. I went ahead and stuck with the format of the other problems by useing a Driver class.
 * This class imports the Geometry class to use its static methods. We prompt the user
 * for a height value and a radius value, then use those two values to calculate the
 * volume and surface area of a sphere, cylinder and cone. The calculations for these are
 * in the Geometry class.
 */
public class Driver {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //create new Geometry object
        Geometry geo = new Geometry();

        //get height and radius from user
        System.out.print("Enter the value for the height: ");
        double h = scan.nextDouble();
        System.out.print("Enter the value for the radius: ");
        double r = scan.nextDouble();

        //print each of the calculations by calling the methods in Geometry class
        System.out.println("The volume of the sphere is: " + geo.sphereVolume(r));
        System.out.println("The surface area of the sphere is: " + geo.sphereSurface(r));
        System.out.println("The volume of the cylinder is: " + geo.cylinderVolume(r, h));
        System.out.println("The surface area of the cylinder is: " + geo.cylinderSurface(r, h));
        System.out.println("The volume of the cone is: " + geo.coneVolume(r, h));
        System.out.println("The surface area of the cone is: " + geo.coneSurface(r, h));
    }
}
