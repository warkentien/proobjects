package P8_14;

import java.util.Scanner;

/**
 * Created by sethf_000 on 10/18/2015.
 *
 * This Class uses 6 static methods to calculate surface area and volume for a
 * sphere, cylinder, and cone. It needs to be imported to other class for use
 */
public class Geometry {


    //return the calculation for volume of a sphere
    public static double sphereVolume(double r){
        return (4.0 / 3.0) * Math.PI * (r * r * r);
    }

    //return the calculation for surface area of a sphere
    public static double sphereSurface(double r){
        return 4 * Math.PI * r * r;
    }

    //return the calculation for volume of a cylinder
    public static double cylinderVolume(double r, double h){
        return Math.PI * r * r * h;
    }

    //return the calculation for surface area of a cylinder
    public static double cylinderSurface(double r, double h){
        return (2 * Math.PI * r * h) + (2 * Math.PI * r * r);
    }

    //return the calculation for volume of a cone
    public static double coneVolume(double r, double h){
        return Math.PI * r * r * (h/3);
    }

    //return the calculation for surface area of a cone
    public static double coneSurface(double r, double h){
        return Math.PI * r * (r + Math.sqrt((h * h) + (r * r)));
    }
}
