package P8_6;

/**
 * Created by sethf_000 on 10/17/2015.
 *
 * This class sets the fuel level and fuel efficiency for a car object and then simulates driving the car
 * while keeping track of how much fuel was used. It can return the remaining amount of fuel in the tank.
 */
public class Car {

    //create the fields for this class
    private double efficiency;
    private double fuel;

    //create the constructor
    public Car(double mpg){
        //set fuel to 0
        this.fuel = 0;
        //declare efficiency in miles/gallon through passed param
        this.efficiency = mpg;
    }

    //method to simulate driving, takes param of distance in miles
    public void drive(int distance){
        //calculate the fuel cost of driving "distance" miles
        fuel = fuel - (distance/efficiency);
    }

    //return the fuel remaining
    public double getGasLevel(){
        return fuel;
    }

    //add new fuel to the car
    public void addGas(double gallons){
        fuel += gallons;
    }



}
