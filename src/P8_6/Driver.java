package P8_6;

/**
 * Created by sethf_000 on 10/17/2015.
 *
 * This program creates a car object that is initialized with a miles/gallon fuel efficiency.
 * It then adds gas amounts by the gallon and simulates driving a certain number of miles.
 * After simulating the driving, we can get how much fuel would be left.
 */
public class Driver {
    public static void main(String[] args) {

        //create new Car object and set its fuel efficiency to 30 mpg
        Car sportsCar = new Car(30);

        //add 20 gallons of gas to our car
        sportsCar.addGas(20);
        //drive the car for 60 miles
        sportsCar.drive(60);

        //print out the remaining gas after driving for 60 miles
        System.out.println("This is the current fuel remaining: " + sportsCar.getGasLevel() + " gallons");

    }
}